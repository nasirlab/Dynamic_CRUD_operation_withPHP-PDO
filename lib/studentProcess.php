<?php 
	include_once("Database.php");
	include_once("session.php");
	Session::init();
	$db = new Database;

	$table = "tbl_student";

	if (isset($_REQUEST['action']) && !empty($_REQUEST['action'])) {
		
		if ($_REQUEST['action'] == 'add'){
			$studentNewData = array(
				'name' => $_POST['name'],
				'email'=> $_POST['email'],
				'phone'=> $_POST['phone']
			);
			$insert = $db->insert($table, $studentNewData);

			if ($insert) {
				$msg = "Data inserted successfully ";
			}else{
				$msg = "Data not inserted  !!!";
			}
			Session::set('msg',$msg);
			$home_url = '../index.php';
			header('Location: '.$home_url);



		}elseif($_REQUEST['action'] == 'edit'){

			$id = $_POST['id'];
			if(!empty($id)){
				$studentData = array(
					'name' => $_POST['name'],
					'email' => $_POST['email'],
					'phone' => $_POST['phone'],
				);
				$table = "tbl_student";

				$condition = array('id'=> $id);
				$update = $db->update($table,$studentData,$condition);

				if ($update) {
					$msg = "Data updated Successfully";
				}else{
					$msg = "Data not updated !";
				}
				Session::set('msg',$msg);
				$home_url = '../index.php';
				header('Location: '.$home_url);
			}
		}elseif($_REQUEST['action'] == 'delete'){

			$id = $_GET['id'];
			if(!empty($id)){

				$table = "tbl_student";

				$condition = array('id'=> $id);

				$delete = $db->delete($table,$condition);

				if ($delete) {
					$msg = "Data deleted Successfully";
				}else{
					$msg = "Data not deleted !";
				}
				Session::set('msg',$msg);
				$home_url = '../index.php';
				header('Location: '.$home_url);
			}
		}
}