<?php
	include_once('inc/header.php');
 ?>
<!-- Main Content Start here-->
<div class="panel panel-default">
	<div class="panel-heading">
		<h3>Add New Student<a class="btn btn-success pull-right" href="index.php">Students list</a></h3>
	</div>
	<div class="panel body">
		<form action="lib/studentProcess.php" method="POST">
			<div class="form-group">
			<label for="name">Student name:</label>
				<input class="form-control" type="text" name="name" id="name">
			</div>			
			<div class="form-group">
			<label for="email">Student email:</label>
				<input class="form-control" type="email" name="email" id="email">
			</div>			
			<div class="form-group">
			<label for="phone">Phone:</label>
				<input class="form-control" type="phone" name="phone" id="phone">
			</div>			
			<div class="form-group">
				<input type="hidden" name="action" value="add">
				<input class="btn btn-primary" type="submit" value="Add Student" name="submit" id="add">
			</div>
		</form>
		
	</div>
	
</div>




			<!-- Main Content End here-->


<?php
	include_once('inc/footer.php'); 
?>